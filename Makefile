essbook: main.o panel.o calendar.o tile.o planner.o event.o 
	g++ main.o panel.o calendar.o tile.o planner.o event.o -o essbook -lncurses
main.o: main.cpp panel/panel.hh panel/calendar.hh 
	g++ -c main.cpp
panel.o: panel/panel.cpp panel/panel.hh panel/event.hh 
	cd panel; g++ -c panel.cpp; mv *.o ..
calendar.o: panel/calendar.cpp panel/calendar.hh panel/panel.hh 
	cd panel; g++ -c calendar.cpp; mv *.o ..
tile.o: panel/tile.cpp panel/tile.hh 
	cd panel; g++ -c tile.cpp; mv *.o ..
event.o: panel/event.cpp panel/event.hh 
	cd panel; g++ -c event.cpp; mv *.o ..
planner.o: panel/event.cpp panel/event.hh panel/planner.cpp panel/planner.hh panel/panel.cpp panel/panel.hh 
	cd panel; g++ -c planner.cpp; mv *.o ..

install:
	mkdir -p $(DESTDIR)/usr/bin
	install -m 0755 essbook $(DESTDIR)/usr/bin/essbook

clean:
	rm *.o Essbook 
