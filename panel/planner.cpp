    /*This file is part of Essbook.

    Essbook is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Essbook is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Essbook.  If not, see <https://www.gnu.org/licenses/>.*/

#include "planner.hh"
#include <sstream>


void Planner::toContents(int mode, vector<int> modeVars){
    resetContents();
    int year = modeVars[0];
    int month = modeVars[1];
    int day = modeVars[2];



    
	
    int origin = 1;
    int tRow = 1;
    int bRow = 1;
    int dRow = 1;
    bool is_true;
    display_num = 0;
    for (int i=target;i<event_list.size();i++){
	is_true = false;
	switch(mode){
	    //month selector
	case 0:
	    if (event_list[i]->getYear() == year && event_list[i]->getMonth() == month){
		is_true = true;
	    }
	    break;
	case 1:
	    if (event_list[i]->getYear() == year && event_list[i]->getMonth() == month && event_list[i]->getDay() == day){
		is_true = true;
	    }
	    break;
	case 2:
	    if (event_list[i]->getYear() == year){
		is_true = true;
	    }
	    break;
	default:
	    is_true = false;
	}
	if (is_true){
	    //title
	    display_num++;
	    tRow = origin;
	    for (int j=0;j<event_list[i]->getTitle().size();j+=8){
		contents[1][tRow].setString(event_list[i]->getTitle().substr(j,j+8));
		if (i == target){
		  contents[1][tRow].setColor(3);
		}
		tRow++;
	    }
	    contents[1][tRow++].setString(to_string(i));
	    //body
	    bRow = origin;
	    for (int j=0;j<event_list[i]->getBody().size();j+=width-15){
		    contents[9][bRow].setString(event_list[i]->getBody().substr(j,j+width-15));
		    bRow++;
	    }
	    //Date
	    dRow = origin;

	    //formatDay
	    ostringstream day;
	    if (event_list[i]->getDay()<10) day << "0";
	    day << to_string(event_list[i]->getDay());
	    
	    //setDay
	    contents[width-3][dRow].setString(day.str());

	    //setMonth
	    contents[width-4][dRow+1].setString(this->shortMonth(event_list[i]->getMonth()));

	    //format Year
	    ostringstream Year;
	    for(int q = to_string(event_list[i]->getYear()).length();q < 4; q++){
		Year << "0";
	    }
	    Year << to_string(event_list[i]->getYear());

	    //setYear
	    contents[width-5][dRow+2].setString(Year.str());

	    //format time
	    ostringstream hour;
	    if (event_list[i]->getHour()<10) hour << "0";
	    hour << to_string(event_list[i]->getHour());
	    ostringstream minute;
	    if (event_list[i]->getMinute()<10) minute << "0";
	    minute << to_string(event_list[i]->getMinute());

	    //set Time
	    contents[width-6][dRow+3].setString(hour.str()+":"+minute.str());
	    dRow += 4;
	    /*if (i == event_list.size()-1){
		return;
		}*/
	}
	
	
	
	origin = max(max(tRow,bRow),dRow)+1;
	if (origin + 4 > height){
	    break;
	}
    }
}


bool Planner::setBindings(int bindings[]){
    up = bindings[0];
    down = bindings[1];
    return true;
}

void Planner::handleKeys(int key){
    if (key == up){
	if (target > 0){
	    target--;
	}
    }else if (key == down){
	if (target < display_num){
	    target++;
	}
    }
}

vector<Event*> Planner::getEventList(){
    return event_list;
}

void Planner::addEvent(){
    event_list.push_back(new Event());
}

void Planner::addEvent(string title, string body, int year, int month, int day, int hour, int minute){
    event_list.push_back(new Event(title, body, year, month, day, hour, minute));
}

bool Planner::deleteEvent(int i){
    if(event_list.size() < i+1) return false; 
    event_list.erase(event_list.begin()+i);
    cout << i << "th event deleted" << endl;
    cin;
    return true;
}

string Planner::shortMonth(int month){
    switch(month){
    case 0:
	return "Jan";
    case 1:
	return "Feb";
    case 2:
	return "Mar";
    case 3:
	return "Apr";
    case 4:
	return "May";
    case 5:
	return "Jun";
    case 6:
	return "Jul";
    case 7:
	return "Aug";
    case 8:
	return "Sep";
    case 9:
	return "Oct";
    case 10:
	return "Nov";
    case 11:
	return "Dec";
    default:
	return "INV";
    }
}

Planner::Planner(int width, int height, int x, int y){
    this->height = height;
    this->width = width;
    this->x = x;
    this->y = y;
    contents.resize(width);
    for (int i = 0; i<width; ++i){
	contents[i].resize(height, *new Tile("", 1));
    }
    target = 0;

    //Default Bindings
    up = KEY_UP;
    down = KEY_DOWN;

}
