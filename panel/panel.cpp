    /*This file is part of Essbook.

    Essbook is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Essbook is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Essbook.  If not, see <https://www.gnu.org/licenses/>.*/

#include "panel.hh"
#include <ncurses.h>


void Panel::makeIntro(){
    int origin = height/2 - 4;
    contents[(width/2) -6][origin].setString("Essbook");
    contents[width/2 - 8][origin+2].setString("version 0.1");
    contents[width/2 - 11][origin+3].setString("GPL-3.0-or-later");
}



/* Change this so it returns a Bool of pass or fail
but it takes a poiter to the panelList and have
these 2 functions manuelly add the new panel (if its created) 
to the panelList */
bool Panel::splitH(vector<Panel*>* list){
    if (height < 20){
	return false;
    }
    height = height/2;
    list->push_back(new Panel(width, height, x, y+height));
    return true;
}

bool Panel::splitV(vector<Panel*>* list){
    if (width <= 70){
	return false;
    }else{
	width = width/2;
	list->push_back(new Panel(width, height, x+width, y));
	return true;
    }
}

void Panel::resetContents(){
    contents.resize(0);
    contents.resize(width);
    for (int i = 0; i<width; ++i){
	contents[i].resize(height, *new Tile("", 1));
    }
}

void Panel::toContents(){
    return;
}

void Panel::addEvent(){
    return;
}
void Panel::addEvent(string title, string body, int year, int month, int day, int hour, int minute){
    return;
}

int Panel::getMode(){
    return -1;
}
vector<int> Panel::getModeVars(){
    return vector<int>();
}

void Panel::handleKeys(int key){
    return;
}

void Panel::reshape(){
    contents.resize(width);
    for (int i=0;i<contents.size();i++){
	contents[i].resize(height);
	for (int j=0;j<contents[i].size();j++){
	    contents[i][j].setString(contents[i][j].getString().substr(0,width-i));
	}
    }
}

void Panel::drawBoarder(string* icon, int color){
    for (int i=0;i<width;i++){
	for (int j=0;j<height;j++){
	    if((i==0 || i==width-1) || (j==0 || j==height-1)){
		setString(i,j,*icon);
		setColor(i,j,color);
	    }
	}
    }
    delete icon;
}

int Panel::getX(){
    return x;
}

int Panel::getY(){
    return y;
}

string Panel::getString(int x, int y){
    return contents[x][y].getString();;
}

void Panel::setString(int x, int y, string icon){
    contents[x][y].setString(icon);
}

int Panel::getColor(int x, int y){
    return contents[x][y].getColor();
}
void Panel::setColor(int x, int y, int color){
    contents[x][y].setColor(color);
}

int Panel::getWidth(){
    return width;
}
int Panel::getHeight(){
    return height;
    }




///CONSTRUCTORS

Panel::Panel(){
    
}
Panel::Panel(int width, int height, int x, int y){
  this->height = height;
  this->width = width;
  this->x = x;
  this->y = y;
  contents.resize(width);
  for (int i = 0; i<width; ++i){
      contents[i].resize(height, *new Tile("", 1));
  }
}


