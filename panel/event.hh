#ifndef EVENT_HH
#define EVENT_HH
#include <iostream>
#include <time.h>
#include <string>
#include "panel.hh"
#include "tile.hh"
#include <ncurses.h>
#include <vector>

using namespace std;

class Event{
public:

    string getTitle();
    string getBody();
    vector<string> getTags();
    void addTag(string tag);
    int getYear();
    int getMonth();
    int getDay();
    int getHour();
    int getMinute();
    void setDate(int year, int month, int day, int hour);
    void setTitle(string title);
    void setBody(string body);
    void setTags(vector<string> tags);

    Event();
    Event(string title, string body, int year, int month, int day, int hour, int minute);
private:
    string title;
    string body;
    vector<string> tags;

    int year;
    int month;
    int day;
    int hour;
    int minute;
};





#endif
