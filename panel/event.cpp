    /*This file is part of Essbook.

    Essbook is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Essbook is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Essbook.  If not, see <https://www.gnu.org/licenses/>.*/

#include "event.hh"

string Event::getTitle(){
    return title;
}

string Event::getBody(){
    return body;
}

vector<string> Event::getTags(){
    return tags;
}

int Event::getYear(){
    return year;
}

int Event::getMonth(){
    return month;
}

int Event::getDay(){
    return day;
}

int Event::getHour(){
    return hour;
}

int Event::getMinute(){
    return minute;
}

void Event::setDate(int year, int month, int day, int hour){
    this->year = year;
    this->month = month;
    this->day = day;
    this->hour = hour;
}

void Event::setTitle(string title){
    this->title = title;
}

void Event::setBody(string body){
    this->body = body;
}

void Event::setTags(vector<string> tags){
    this->tags = tags;
}

void Event::addTag(string tag){
    tags.push_back(tag);
}


Event::Event(string title, string body, int year, int month, int day, int hour, int minute){
    this->title = title;
    this->body = body;
    this->year = year;
    this->month = month;
    this->day = day;
    this->hour = hour;
    this->minute = minute;
}

Event::Event(){
    title = "DEFAULT EVENT";
    body = "This is a event created with no arguements";
    year = 1970;
    month = 0;
    day = 1;
    hour = 0;
    minute = 0;
}

