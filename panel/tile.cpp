    /*This file is part of Essbook.

    Essbook is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Essbook is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Essbook.  If not, see <https://www.gnu.org/licenses/>.*/

#include "tile.hh"

string Tile::getString(){
    return contents;
}
int Tile::getColor(){
    return color;
}

void Tile::setString(string contents){
    this->contents = contents;
}
void Tile::setColor(int color){
    this->color = color;
}

Tile::Tile(string contents, int color){
    this->contents = contents;
    this->color = color;
}

Tile::Tile(){
    contents = " ";
    color = 1;
}
