#ifndef CALENDAR_HH
#define CALENDAR_HH
#include <iostream>
#include <time.h>
#include "panel.hh"
#include "tile.hh"
#include <ncurses.h>
#include <vector>
#include <sstream>


using namespace std;

class Calendar: public Panel{
public:
    virtual vector<int> getModeVars();
    virtual int getMode();
    virtual void toContents();
    virtual void handleKeys(int key);

    //time variables
    void updateTime();
    int getSecond();
    int getMinute();
    int getHour();
    int getDayOfMonth();
    int getMonth();
    int getYear();
    int getDayOfWeek();
    int getDayOfYear();
    int getDaysInMonth(int month);

    //Key Bindings
    bool setBindings(int bindings[5]);
    int mode_swap;
    int up;
    int down;
    int left;
    int right;
    
    
    Calendar(int width, int height, int x, int y);
protected:
    void switchMode();

    void makeGrid();
    string getMonthString(int month);
    string tDay;
    vector<vector<string> > grid;
    time_t unix_time;
    tm* ltm;
    int second_after_minute;
    int minute;
    int hour;
    int day_of_month;
    int month; 
    int year; ///since 1900
    int day_of_week; /// days since sunday
    int day_of_year;

    int mode; //0:month 1:day
    int view_year;
    int view_month;
    int view_day;
};








#endif
