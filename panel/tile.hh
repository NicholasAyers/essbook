#ifndef TILE_HH
#define TILE_HH

#include "string"
#include "iostream"

using namespace std;


class Tile{
public:
    string getString();
    int getColor();
    void setString(string contents);
    void setColor(int color);

    Tile();
    Tile(string contents, int color);
protected:
    string contents;
    int color;
};

#endif
