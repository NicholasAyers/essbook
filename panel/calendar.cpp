    /*This file is part of Essbook.

    Essbook is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Essbook is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Essbook.  If not, see <https://www.gnu.org/licenses/>.*/

#include "calendar.hh"
using namespace std;



void Calendar::switchMode(){
    mode==0?mode=2:mode==1?mode=0:mode=1;
    makeGrid();
    view_day = 1;
}

bool Calendar::setBindings(int bindings[5]){
    mode_swap = bindings[0];
    up = bindings[1];
    down = bindings[2];
    left = bindings[3];
    right = bindings[4];
    return true;
}

void Calendar::handleKeys(int key){
    if (key == mode_swap){
	switchMode();
	return;
    }else if(key == up){
	if (mode == 1 && view_day > 7){
	    view_day -= 7;
	}else if (mode == 0 && view_month > 3){
	    view_month -= 4;
	}else if (mode == 2){
	    view_year--;
	}
    }else if (key == down){
	if (mode == 1 && view_day < getDaysInMonth(view_month)-7){
	    view_day += 7;
	}else if (mode == 0 && view_month < 8){
	    view_month += 4;
	}else if (mode == 2){
	    view_year++;
	}
    }else if (key == left){
	if (mode == 1 && view_day >1){
	    view_day--;
	}else if (mode == 0 && view_month >0){
	    view_month--;
	}
    }else if (key == right){
	if (mode == 1 && view_day <getDaysInMonth(view_month)){
	    view_day++;
	}else if (mode == 0 && view_month < 11){
	    view_month++;
	}
    }else{
	return;
    }
    makeGrid();
    return;
}

void Calendar::makeGrid(){
    if (mode == 0){
	grid.resize(0);
	for (int i=0;i<3;i++){
	    grid.push_back(*new vector<string>);
	}
	grid[0].push_back("J");
	grid[0].push_back("F");
	grid[0].push_back("M");
	grid[0].push_back("A");
	grid[1].push_back("M");
	grid[1].push_back("J");
	grid[1].push_back("J");
	grid[1].push_back("A");
	grid[2].push_back("S");
	grid[2].push_back("O");
	grid[2].push_back("N");
	grid[2].push_back("D");
    }else if (mode == 1){
	//set grid to be 6 wide
	grid.resize(0);
	for (int i=0;i<6;i++){
	    grid.push_back(*new vector<string>);
	}
	//Fint hat day of the week the first is on 
	int y = view_year;
	int m = view_month + 1;
	int d = 1;
	static int t[] = {0,3,2,5,0,3,5,1,4,6,2,4};
	y -= m<3;
	int firstDay = (y + y/4 -y/100 + y/400 + t[m-1] + d) % 7;
	
	for (int i=0;i<firstDay;i++){
	    grid[0].push_back("--");
	}
	
	//Add all the days of the month in
	int week = 0;
	for (int i=1;i<=getDaysInMonth(view_month);i++){
	    stringstream ss;
	    ss << i;
	    string sDay;
	    ss >> sDay;{
		int str_length = sDay.length();
		for (int i=0;i<2-str_length;i++){
		    sDay = "0" + sDay;
		}}
	    if (i == day_of_month){
		tDay = sDay;
	    }
	    grid[week].push_back(sDay);
	    int str_length = sDay.length();

	    
	    
	    
	    
	///grid[week].push_back //add string formating
	    if(grid[week].size() == 7){
		week++;
	}
	}
	//Reshape grid to be  7x5
	for (int i=0;i<grid.size();i++){
	    grid[i].resize(7,"--");
	}
    }else if (mode == 2){
	grid.resize(0);
	for (int i=0;i<3;i++){
	    grid.push_back(*new vector<string>);
	}
	for (int i=0;i<3;i++){
	    grid[i].push_back(to_string(view_year+i-1));
	}
    }
}

string Calendar::getMonthString(int month){
    switch(month){
    case 0:
	return "January";
    case 1:
	return "Febuary";
    case 2:
	return "Mach";
    case 3:
	return "April";
    case 4:
	return "May";
    case 5:
	return "June";
    case 6:
	return "July";
    case 7:
	return "August";
    case 8:
	return "September";
    case 9:
	return "October";
    case 10:
	return "November";
    case 11:
	return "December";
    Default:
	return "ERR:NoMonth";
	
    }
    return "ERR:NOMONTH";
}

void Calendar::toContents(){

    resetContents();
    
    if (mode == 1){
	
	contents[6][1].setString(getMonthString(view_month)); //Make function to return name of month
	contents[1][1].setString(to_string(year));
    }
    int l = 1; //use l to add space between days
    for (int i=0;i<grid.size();i++){
	l = 1;
	for (int j=0;j<grid[i].size();j++){
	    contents[l][i+2].setString(grid[i][j]);
	    stringstream s(grid[i][j]);
	    int x;
	    s >> x;
	    if (contents[l][i+2].getString() == tDay && view_month == month && view_year == year){
		contents[l][i+2].setColor(2);
	    }else if (mode == 0 && i*4 +j == view_month){
		contents[l][i+2].setColor(3);
	    }else if (mode == 1 && x == view_day){
		contents[l][i+2].setColor(3);
	    }else if (mode == 2 && x == view_year) {
		contents[l][i+2].setColor(3);
	    }else{
		contents[l][i+2].setColor(1);
	    }
	    //cout << "x: " << l << "; Y: " << i+1 << endl << "    " << contents[l][i+1] << endl;
	    l += 3; // because each day is 2 and 1 space between each day
	}
	
    }
}

void Calendar::updateTime(){
    unix_time = time(0);
    second_after_minute = ltm->tm_sec;
    minute = ltm->tm_min;
    hour = ltm->tm_hour;
    day_of_month = ltm->tm_mday;
    month = ltm->tm_mon;
    year = 1900 + ltm->tm_year;
    day_of_week = ltm->tm_wday;
    day_of_year = ltm->tm_yday;
}

int Calendar::getMode(){
    return mode;
}

vector<int> Calendar::getModeVars(){
    vector<int> vect{view_year,view_month,view_day};
    return vect;
}

int Calendar::getDaysInMonth(int month){
    if (month == 1 || month == 2 || month == 4 || month == 6 || month == 7 || month == 9 || month == 11 ){
	return 31;
    }else if (month == 1){
	if (year % 4 == 0){
	    if ((year % 400 == 0) && (year % 100 == 0 ) || (year % 400 != 0)){
		return 29;
	    }
	}
	return 28;
    }else{
	return 30;
    }
}

int Calendar::getSecond(){
    return second_after_minute;
}
int Calendar::getMinute(){
    return minute;
}
int Calendar::getHour(){
    return hour;
}
int Calendar::getDayOfMonth(){
    return day_of_month;
}
int Calendar::getMonth(){
    return month;
}
int Calendar::getYear(){
    return year;
}
int Calendar::getDayOfWeek(){
    return day_of_week;
}
int Calendar::getDayOfYear(){
    return day_of_year;
}


Calendar::Calendar(int width, int height, int x, int y ){
    this->height = height;
    this->width = width;
    this->x = x;
    this->y = y;
    mode = 0;
    contents.resize(width);
    for (int i = 0; i<width; ++i){
	contents[i].resize(height, *new Tile("", 1));
    }
    unix_time = time(0);
    ltm = localtime(&unix_time);
    updateTime();
    makeGrid();
    view_year = year;
    view_month = month;
    view_day = day_of_month;

    //default key bindings
    mode_swap = KEY_HOME;
    up = KEY_UP;
    down = KEY_DOWN;
    left = KEY_LEFT;
    right = KEY_RIGHT;

}
