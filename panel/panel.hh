#ifndef PANEL_H
#define PANEL_H

#include <iostream>
#include <vector>
#include <string>
#include "tile.hh"
#include "event.hh"
#include "../config/bindings.hh"
using namespace std;



class Panel{
public:
    vector<vector<Tile> > contents;

    void makeIntro();
    void drawBoarder(string* icon, int color);
    bool splitH(vector<Panel*>* list);
    bool splitV(vector<Panel*>* list);

    void reshape();
    
    virtual void toContents();
    virtual void addEvent();
    virtual void addEvent(string title, string body, int year, int month, int day, int hour, int minute);
    virtual int getMode();
    virtual vector<int> getModeVars(); 
    virtual void handleKeys(int key);



    
    int getX();
    int getY();
    int getWidth();
    int getHeight();
    string getString(int x, int y);
    void setString(int x,int y,string icon);
    int getColor(int x, int y);
    void setColor(int x, int y, int color);
    
    
    Panel();
    Panel(int width, int height, int x, int y);
protected:
    int width;
    int height;
    int x;
    int y;
    void resetContents();
};




#endif
