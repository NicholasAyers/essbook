#ifndef PLANNER_HH
#define PLANNER_HH
#include <iostream>
#include <time.h>
#include "panel.hh"
#include "tile.hh"
#include "event.hh"
#include <string>
#include <ncurses.h>
#include <vector>

using namespace std;

class Planner: public Panel{
public:
    virtual void handleKeys(int key);
    void toContents(int mode, vector<int> modeVars);
    vector<Event*> getEventList();
    virtual void addEvent();
    virtual void addEvent(string, string, int, int, int, int, int);
    bool deleteEvent(int i);
    Planner(int width, int height, int x, int y);

    //Key Bindings
    bool setBindings(int bindings[2]);
    int up;
    int down;
    
    string shortMonth(int month);
private:
    vector<Event*>  event_list;
    int target;
    int display_num;
    };

#endif


