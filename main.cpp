/*This file is part of Essbook.

    Essbook is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Essbook is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Essbook.  If not, see <https://www.gnu.org/licenses/>.*/

#include <iostream>
#include <ncurses.h>
#include "panel/panel.hh"
#include <string>
#include <fstream>
#include "panel/calendar.hh"
#include "panel/planner.hh"
#include "panel/event.hh"
#include <stdlib.h>
using namespace std;

bool loadConfigFile(string homeDir, int *baseKeys[]);
bool flags(int argc, char *argv[]);
bool loadDataFile(string homeDir);
bool processCommand(string *commandTitle, string *command, int *cmdid, vector<string> *args);

vector<Panel*> panelList;
Calendar Cal(40,24,0,0);
Planner Plan(40,24,40,0);
bool new_event = false;

int main(int argc, char *argv[]){
    panelList.push_back(&Cal);
    panelList.push_back(&Plan);
    const string homeDir = string(getenv("HOME"));

    //Key vaiables
    int QUIT_KEY;
    int SUBMIT_KEY;
    int *keyArr[2] = {&QUIT_KEY, &SUBMIT_KEY};

    
    //load file and process input flags
    loadConfigFile(homeDir, keyArr);
    loadDataFile(homeDir);
    if (!flags(argc, argv)) return 0;

    
    
    string command = "";
    string commandTitle = "?";
    int cmdid = -1;
    vector<string> commandArgs;
    bool typing = false;

    
    
    

    
    //start ncurses
    initscr();
    keypad(stdscr, true);
    noecho();
    start_color();  
    init_pair(1, COLOR_CYAN, COLOR_BLACK);
    init_pair(2, COLOR_BLACK, COLOR_RED);
    init_pair(3, COLOR_WHITE, COLOR_RED);
    
    Panel Intro(80,24,0,0);
    clear();
    Intro.makeIntro();
    Intro.drawBoarder(new string("#"), 3);
    for (int i=0;i<Intro.getWidth();i++){
	for (int j=0;j<Intro.getHeight();j++){
	    attron(COLOR_PAIR(Intro.getColor(i,j)));
	    
	    mvprintw(j,i,Intro.getString(i,j).c_str());
	    attroff(COLOR_PAIR(Intro.getColor(i,j)));
	}
    }
    getch();
    //run loop
    bool quit = new_event==true?true:false;
    while(quit == false){
	clear();
	Cal.toContents();
	Plan.toContents(Cal.getMode(), Cal.getModeVars());
	for(int i=0;i<panelList.size();i++){
	    panelList[i]->reshape();
	    panelList[i]->drawBoarder(new string("$"), i==0?3:1);
	}
	for (int k=0;k<panelList.size();k++){
	    int ox = panelList[k]->getX();
	    int oy = panelList[k]->getY();
	    for (int i=0;i<panelList[k]->getWidth();i++){
		for (int j=0;j<panelList[k]->getHeight();j++){
		    attron(COLOR_PAIR(panelList[k]->getColor(i,j)));
		    
		    mvprintw(j+oy,i+ox,panelList[k]->getString(i,j).c_str());
		    attroff(COLOR_PAIR(panelList[k]->getColor(i,j)));
		}
	    }
	}
	if (typing){
	    mvprintw(23,1,(commandTitle+":"+command).c_str());
	}
	refresh();
	int key = getch();

	//Commands
	if (key == SUBMIT_KEY){
	    typing = !processCommand(&commandTitle, &command, &cmdid, &commandArgs);       
	}else if (typing){
	    if (key == 127){
		command = command.substr(0,command.length()-1);
	    }else{
		command += (char)key;
	    }
	}else if(key == PANEL_LEFT){
		panelList.push_back(panelList[0]);
		panelList.erase(panelList.begin());
	}else if (key ==PANEL_RIGHT){
		panelList.insert(panelList.begin(),panelList.back());
		panelList.pop_back();
	}else if(key == QUIT_KEY){
	    quit = true;
	}else{ 
	    panelList[0]->handleKeys(key);
	}
    }
    endwin();
    ofstream saveFile;
    saveFile.open(homeDir + "/.local/share/essbook/save", ios::out | ios::trunc);
    vector<Event*> eList = Plan.getEventList();
    for (int i=0; i<eList.size();i++){
	saveFile << eList[i]->getTitle() << "\n" << eList[i]->getBody() << "\n";
	saveFile << eList[i]->getYear() << "\n" << eList[i]->getMonth() << "\n" << eList[i]->getDay() << "\n";
	saveFile << eList[i] ->getHour() << "\n" << eList[i] ->getMinute() <<"\n" << "\n";
    }
    saveFile.close();
    return 0;
}


bool loadConfigFile(string homeDir, int *baseKeys[]){
    /*
    | Index | Name   |
    |     0 | QUIT   |
    |     1 | SUBMIT |*/

    ifstream loadFile(homeDir +"/.config/essbook/config");
    string line;
    int mode_swap, up, down, left, right;
    up = KEY_UP;
    down = KEY_DOWN;
    left = KEY_LEFT;
    right = KEY_RIGHT;
    mode_swap = KEY_HOME;
    *baseKeys[0] = (int)'q';
    *baseKeys[1] = 10;
    if (loadFile.is_open()){
	while(getline(loadFile, line)){
	    if (line == "UP:"){
		getline(loadFile,line);
		if(line.substr(0,5) == "(int)"){
		    up = (int)line.at(5);
		}else up = atoi(line.c_str());
	    }else if (line == "DOWN:"){
		getline(loadFile,line);
		if(line.substr(0,5) == "(int)"){
		    down = (int)line.at(5);
		}else down = atoi(line.c_str());
	    }else if (line == "LEFT:"){
	        getline(loadFile,line);
		if(line.substr(0,5) == "(int)"){
		    left = (int)line.at(5);
		}else left = atoi(line.c_str());
	    }else if (line == "RIGHT:"){
	        getline(loadFile,line);
		if(line.substr(0,5) == "(int)"){
		    right = (int)line.at(5);
		}else right = atoi(line.c_str());
	    }else if (line == "MODE_SWAP:"){
	        getline(loadFile,line);
		if(line.substr(0,5) == "(int)"){
		    mode_swap = (int)line.at(5);
		}else mode_swap = atoi(line.c_str());
	    }else if (line == "QUIT:"){
		getline(loadFile,line);
		if(line.substr(0,5) == "(int)"){
		    *baseKeys[0] = (int)line.at(5);
		}else *baseKeys[0] = atoi(line.c_str());
	    }else if (line == "SUBMIT:"){
		getline(loadFile,line);
		if(line.substr(0,5) == "(int)"){
		    *baseKeys[1] = (int)line.at(5);
		}else *baseKeys[1] = atoi(line.c_str());
	    }
	    
	}
	int cats[2] = {up,down};
	Plan.setBindings(cats);
    }else{
	cout << "die" << endl;
    }
    return true;
}

bool loadDataFile(string homeDir){
    string fs;
    string line;
    ifstream loadFile(homeDir + "/.local/share/essbook/save");
    int stage = 0;
    if (loadFile.is_open()){
	string t;
	string b;
	int y;
	int mo;
	int d;
	int h;
	int mi;
	while(getline(loadFile, line)){
	    switch (stage){
	    case 0:
		t = line;
		break;
	    case 1:
		b = line;
		break;
	    case 2:
		y = atoi(line.c_str());
		break;
	    case 3:
		mo = atoi(line.c_str());
		break;
	    case 4:
		d = atoi(line.c_str());
	    case 5:
		h = atoi(line.c_str());
		break;
	    case 6:
		mi = atoi(line.c_str());
		break;
	    case 7:
		stage = -1;
		Plan.addEvent(t, b, y, mo, d, h, mi);
		break;
	    }
	    stage++;
	}
    }else {
	cout << "~/.local/share/essbook not found" << endl << "ensure this directory exists to make use of this program" << endl;
	
	new_event = true;
    }
    return true;
}

bool flags(int argc, char *argv[]){
    cout << argv[0] << endl;
    cout << argv[1] << endl;
    for (int i=1;i<argc;i++){
	char *flag = argv[i];
	if (flag == string("new")){
	    if (argc != 9){
		cout << "invalid number of arguments(" << argc << ") for new event(should be 9)" << endl;
		return false;
	    }
	    new_event = true;
	    string new_title = argv[2];
	    string new_body = argv[3];
	    int new_year = stoi(argv[4]);
	    int new_month = stoi(argv[5]);
	    int new_day = stoi(argv[6]);
	    int new_hour = stoi(argv[7]);
	    int new_minute = stoi(argv[8]);

	    Plan.addEvent(new_title, new_body, new_year, new_month, new_day, new_hour, new_minute);
	    break;
	}else if (flag == string("del") || (flag == string("delete"))){
	    if (argc != 3){
		cout << "invalid number of arguments(" << argc << ") for delete event(should be 3)" << endl;
		return false;
	    }
	    if (!Plan.deleteEvent(stoi(argv[2]))){
		cout << "event list does not contain a " << argv[2] << "th member" << endl;
		return false;
	    }
	    break;
	}else{
	    cout << "unrecognized input flag" << endl;
	    return false;
	}
    }
    return true;
}

bool processCommand(string *commandTitle, string *command, int *cmdid, vector<string> *args){
    /*return value is true if the command is done else it is false
     | id | meaning      |
     | -1 | new command  |
     | 01 | new event    |
     | 02 | delete event |
     |    |              |

    
     */
    int next = args->size();

    //new command set id
    if (*cmdid == -1){
	if (*command == "new"){
	    *cmdid = 1;
	}else if (*command == "delete"){
	    *cmdid = 2;
	}else if (*command != ""){
	    *cmdid = -1;
	    *command = "";
	    *commandTitle = "?";
	    return true;
	} 
    }
    //New command
    switch(*cmdid){
    case 1:
	if (*command == "new"){
	    *commandTitle = "Title";
	    *command = "";
	}else{
	    (*args).push_back(*command);
	    if(*commandTitle == "Title")*commandTitle = "Body";
	    else if(*commandTitle == "Body")*commandTitle = "Year";
	    else if(*commandTitle == "Year")*commandTitle = "Month";
	    else if(*commandTitle == "Month")*commandTitle = "Day";
	    else if(*commandTitle == "Day")*commandTitle = "Hour";
	    else if(*commandTitle == "Hour")*commandTitle = "Minute";
	    *command = "";
	}
	if (args->size() == 7){
	    Plan.addEvent((*args)[0],(*args)[1],stoi((*args)[2]),stoi((*args)[3]),stoi((*args)[4]),stoi((*args)[5]),stoi((*args)[6]));
	    
	    args->resize(0);
	    *cmdid = -1;
	    *commandTitle = "?";
	    return true;
	}
	//Delete command
	break;
    case 2:
	if(*command == "delete"){
	    *commandTitle = "event id";
	    *command = "";
	}else{
	    Plan.deleteEvent(stoi(*command));
	    args->resize(0);
	    *cmdid = -1;
	    *commandTitle = "?";
	    return true;
	}
	
    }
    return false;
}
